package com.company;

/**
 * Created by Albert on 17.05.2016.
 */
public enum XorO {
    O('O'),X('X'),empty(' ');
    private char mychar;
    XorO(char o) {
        mychar = o;
    }
    public char ToString(){
        return mychar;
    }
}
